﻿using UnityEngine;
using System.Collections;

public class script : MonoBehaviour {

	public float points = 20;
	public float min_radius = .3f;
	public float max_radius = .5f;
	public float speed = 2;

	public int direction = 1;


	// Use this for initialization
	void Start () {


		float step_radius = (max_radius - min_radius) / points;


		ParticleSystem ps = GetComponent<ParticleSystem> ();

		var vel = ps.velocityOverLifetime;

		vel.enabled = true;

		vel.space = ParticleSystemSimulationSpace.Local;

		ps.startSpeed = 0;


		//vel.y = new ParticleSystem.MinMaxCurve(0.5f);


		AnimationCurve x_axis = new AnimationCurve ();
		AnimationCurve y_axis = new AnimationCurve ();

		float radius = min_radius;
		for (int i = 0; i < points; i++) {

			Keyframe key = new Keyframe ();

			key.time = (i / (points - 1));

			key.value = direction * Mathf.Sin (key.time * (2 / radius ) * Mathf.PI);
			x_axis.AddKey (key);


			key.value = direction * Mathf.Cos (key.time * (2 / radius) * Mathf.PI);
			y_axis.AddKey (key);

			radius += step_radius;

		}

		vel.x = new ParticleSystem.MinMaxCurve (speed, x_axis);
		vel.z = new ParticleSystem.MinMaxCurve (speed, y_axis);

			
	}
	
	// Update is called once per frame
	void Update () {

		//transform.Rotate(new Vector3(0, 0, 1), 1);
		transform.Rotate(new Vector3(0, 1, 0), -.5f);
	
	}
}
