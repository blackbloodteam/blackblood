﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

    public float speed;

    // Damage is filled by the gun
    public float damage;




    // Update is called once per frame
    void Update () {

        transform.position += transform.forward * speed * Time.deltaTime;

    }

    //public void OnTriggerEnter(Collider other)
    //{
    //    Debug.Log(other.gameObject);
    //}




}
