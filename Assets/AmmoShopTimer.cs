﻿using UnityEngine;
using System.Collections;

public class AmmoShopTimer : MonoBehaviour {

	public AmmoShop shop;

	CapsuleCollider collider;

	public float time;

	//private bool isEnabled;

	private bool started = false;
	private float nextTime;



	public void Play(bool startStatus){

		started = true;
		nextTime = Time.time + time;
		collider.enabled = startStatus;
	
	}



	void Start(){

		collider = GetComponent<CapsuleCollider>();

		Play (true);

	}


	void Update(){


		if (started) {


			if (Time.time >= nextTime) {

				collider.enabled = !collider.enabled;

				nextTime = Time.time + time;

			}
		}
	}
}
