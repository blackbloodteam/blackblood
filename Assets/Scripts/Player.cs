﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

public class Player : MonoBehaviour {

    public Image healthBar;

    public class PointsMultiplier
    {

        public bool doubleMultiplier = false;
        public bool tripleMultiplier = false;
        public bool quadMultiplier = false;

        public int currentMultiplier = 1;


        public void AdjustMultiplier()
        {
            currentMultiplier = 1;

            if (doubleMultiplier)
                currentMultiplier = 2;

            if (tripleMultiplier)
                currentMultiplier = 3;

            if (quadMultiplier)
                currentMultiplier = 4;
        }

    }

    private const int MAX_HEALTH = 100;
    private const int MIN_HEALTH = 0;

    private PointsMultiplier multiplier;


    public int score = 0;

    //public 
    public GameObject mygun;
    public GameManager gameManager;
    public Canvas canvas;

    private int health;

    public Transform gunSpot;

	// Use this for initialization
	void Start () {

        health = MAX_HEALTH;

        multiplier = new PointsMultiplier();

        canvas.UpdateHealthText(health);


    }
	
	// Update is called once per frame
	void Update () {

        if (gameManager.GAMEOVER)
            return;


        if (health <= 0)
        {
            gameManager.GAMEOVER = true;

            canvas.panels.inGamePanel.SetActive(false);


            canvas.SetRoundsSurvivedText(gameManager.level);
            canvas.panels.gameOverPanel.SetActive(true);

            GetComponent<FirstPersonController>().getMouseLook().SetCursorLock(false);
            GetComponent<FirstPersonController>().enabled = false;

        }
	
	}

    public void RestoreLife()
    {

        health = MAX_HEALTH;
        healthBar.fillAmount = 1;
        canvas.UpdateHealthText(health);

    }

    public void AddScore(int _points)
    {

        int points;

        // Prevent buying gun using multiplier
        if (_points <= 0)
            points = _points;
        else
            points = _points * multiplier.currentMultiplier;


        score += points;

        canvas.UpdateScoreText(score);         // Update Canvas

        canvas.addScoreTextAnimation(points);

    }

    public int GetScore()
    {
        return this.score;
    }


    public void SetGun(GameObject gun)
    {

		if (mygun != null)
			Destroy (mygun.gameObject);

        Vector3 position = gun.transform.position;
        Quaternion rotation = gun.transform.rotation;
        mygun = Instantiate(gun, gunSpot.position, new Quaternion()) as GameObject; // Add new Gun;
        mygun.transform.parent = gunSpot;     
        mygun.transform.localPosition = position;
        mygun.transform.localRotation = rotation;

    }
    

    public void setPointsPerk(PerkType perk, bool isActive)
    {

        switch (perk)
        {

            case PerkType.DOUBLE_POINTS:
                multiplier.doubleMultiplier = isActive;
                break;

            case PerkType.TRIPLE_POINTS:
                multiplier.tripleMultiplier = isActive;
                break;

            case PerkType.QUAD_POINTS:
                multiplier.quadMultiplier = isActive;
                break;

        }

        multiplier.AdjustMultiplier();
    }

    public void takeDamage(int damage)
    {
        if (!isDead())
        {
            health -= damage;
            healthBar.fillAmount -= (float)damage/100.0f;
            canvas.UpdateHealthText(health);
        }
        else
        die();
    }

    public bool isDead()
    {
        return health == 0;
    }

    public void die()
    {
        if (health <= 0)
        {
            health = 0;
        }
        Debug.Log("i'm dead");
        
    }
}
