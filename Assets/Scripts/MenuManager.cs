﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour {

    //public GameObject Menu;
    [System.Serializable]
    public class LoadingScreen
    {
        public GameObject screen;
        public Image loadingImage;

    }


    public Menu currentMenu;

    public LoadingScreen loadingScreen;




    public void Start()
    {

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        ShowMenu(currentMenu);

        //Menu.SetActive(true);
        //Loading.SetActive(false);
    }

    public void ShowMenu(Menu menu)
    {

        if (currentMenu != null)
            currentMenu.IsOpen = false;


        currentMenu = menu;
        currentMenu.IsOpen = true;
        
    }


    public void NewGame()
    {

        currentMenu.gameObject.SetActive(false);
        loadingScreen.screen.SetActive(true);

        StartCoroutine("LoadScene");

    }

    IEnumerator LoadScene()
    {

        loadingScreen.loadingImage.fillAmount = 0.0f;

        //yield return new WaitForSeconds(1);

        AsyncOperation ao = SceneManager.LoadSceneAsync("StartingZone");
        ao.allowSceneActivation = false;

        while (!ao.isDone)
        {

            loadingScreen.loadingImage.fillAmount = ao.progress;

            if (ao.progress == 0.9f)
            {
                ao.allowSceneActivation = true;
            }

            Debug.Log(ao.progress);

            yield return null;
        }
    }


    public void ExitGame()
    {
        Application.Quit();
    }


}
