﻿using UnityEngine;
using System.Collections;

public class Sphere : MonoBehaviour {

    private GameManager gameManager;

	// Use this for initialization
	void Start () {

        // Get the Manager Script
        this.gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();

	}

    void OnTriggerEnter(Collider other)
    {

        if ( other.CompareTag("Player") ){

            this.gameManager.CollectSphere();

            Destroy(this.gameObject);

        }

    }


}
