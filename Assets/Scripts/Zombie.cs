﻿using UnityEngine;
using System.Collections;
using System;

public class Zombie : Enemy
{

    private float BASE_HEALTH = 50;

    // Use this for initialization
    void Awake()
    {

    }


	public override void StartEnemy (int round)
	{

		dyingAnimation = "fall";

        if (round < 20)
        {
            health = BASE_HEALTH * round/2;
            navAgent.speed += 0.1f * round/2;

        }
        else
        {
            health = BASE_HEALTH * 20 / 2;
            navAgent.speed += 0.1f * 20 / 2;
        }

        if (anim.GetCurrentAnimatorStateInfo(0).IsName("walk"))
        {

            if (round < 20)
            {
                anim.speed += 0.1f * round;

            }
            else
            {
                anim.speed += 0.1f * 20;
            }

            
        }
        attackDistance = 1.2f;
		
	}

    // Update is called once per frame
    void Update()
    {

        if (gameManager.GAMEOVER)
        {
            navAgent.Stop();
            return;
        }
            


        if (alive)
        {
            navAgent.SetDestination(player.transform.position);
        
            if(attackInRange())
            {
                anim.Play("attack");
            }
            else
            {
                if (!anim.GetCurrentAnimatorStateInfo(0).IsName("attack"))
                    anim.Play("walk");
            }

        }
        else
        {

            if (!dropCheck)
            {

                if (!anim.GetCurrentAnimatorStateInfo(0).IsName("fall"))
                {

                    dropManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<Drop>();

                    // Check Drop
                    Perk perk = dropManager.getDrop(dropRate);

                    // Do not check it again
                    dropCheck = true;


                    if (perk != null && perk.weight != PerkWeight.NONE)
                    {

                        Instantiate(perk.gameObject, transform.position + Vector3.up, perk.gameObject.transform.rotation);

                    }
                }
            }
        }
    }




    protected override AudioClip GetAudioClip(SOUND_TYPES type)
    {

        return soundmanager.GetClip(GetKey(type), SoundManager.SoundList.ZOMBIE);

    }


}
