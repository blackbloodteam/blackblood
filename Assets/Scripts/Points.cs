﻿using UnityEngine;
using System.Collections;

public enum Points
{
    HEADSHOT = 100,

    HIT = 10,

    KILL = 50,

    SPHERE_COLLECT = 20

}