﻿using UnityEngine;
using System.Collections;
using System;

public class Ogre : Enemy
{
    [Serializable]
    public class randomHpRegen
    {
        public float min;
        public float max;

    }
    public randomHpRegen regenRange;

    private bool done_regenerating;
    private bool regenerationOnCooldown;
    private float BASE_HEALTH = 100;
    private bool isSpawning;

    public override void StartEnemy(int round)
    {
        attackDistance = 3.0f;

        regenerationOnCooldown = false;
        done_regenerating = false;

        dyingAnimation = "Die";


        level = (round < 20) ? round : 20;
        
        health = BASE_HEALTH * (level / 2.0f);


        navAgent.speed = 4;
        //navAgent.speed += round;

        isSpawning = true;

    }


    private void beginRegeneration()
    {
        navAgent.Stop();
        source.PlayOneShot(GetAudioClip(SOUND_TYPES.ROAR));
        anim.Play("Roar");
    }

    public void spawnFinished()
    {
        isSpawning = false;
    }

    private void regenerateHealth()
    {
        health = UnityEngine.Random.Range(regenRange.min, regenRange.max);

    }


    private IEnumerator regenerationCooldown()
    {
        regenerationOnCooldown = true;
        yield return new WaitForSeconds(20);
        regenerationOnCooldown = false;
    }
    // Update is called once per frame
    void Update()
    {

        if (gameManager.GAMEOVER)
        {
            navAgent.Stop();
            return;
        }

        if (alive && !isSpawning)
        {
            navAgent.SetDestination(player.transform.position);


            float x = Mathf.Clamp(health, 0, 150);

            if (!anim.GetCurrentAnimatorStateInfo(0).IsName("Roar"))
            {
                //if (Mathf.Clamp(health, 0, 150) == health && !regenerationOnCooldown)
                Debug.Log(health + " - " + BASE_HEALTH * (level / 4.0f));
                if (Mathf.Clamp(health, 0, BASE_HEALTH * (level / 4.0f)) == health && !regenerationOnCooldown)
                {
                    beginRegeneration();
                }
                else
                {
                    navAgent.Resume();

                    if (attackInRange())
                    {
                        navAgent.Stop();
                        anim.Play("Attack");
                    }
                    else
                    {
                        navAgent.Resume();
                        if (!anim.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
                            anim.Play("Run");
                    }
                }
            }
        }

        else if (!isSpawning)
        {

            if (!dropCheck)
            {

                if (!anim.GetCurrentAnimatorStateInfo(0).IsName("Die"))
                {

                    dropManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<Drop>();

                    // Check Drop
                    Perk perk = dropManager.getDrop(dropRate);

                    // Do not check it again
                    dropCheck = true;


                    if (perk != null && perk.weight != PerkWeight.NONE)
                    {

                        Instantiate(perk.gameObject, transform.position + Vector3.up, perk.gameObject.transform.rotation);

                    }
                }
            }
        }
    }



    protected override AudioClip GetAudioClip(SOUND_TYPES type)
    {

        return soundmanager.GetClip(GetKey(type), SoundManager.SoundList.OGRE);
    }


}
