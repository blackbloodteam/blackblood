﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {

    private Animator animator;
    private CanvasGroup canvasGroup;

    //public bool IsOpen
    //{
    //    get { return animator.GetBool("IsOpen"); }
    //    set { animator.SetBool("IsOpen",value); }

    //}

    public bool IsOpen
    {
        get { return gameObject.activeSelf; }
       // set { animator.SetBool("IsOpen", value); }
        set { gameObject.SetActive(value);

            if (value == true)
            {

                if (gameObject.name != "Loading")
                    StartCoroutine(FadeIn(0.5f));

            }
        }
    }




    public IEnumerator LoadScene()
    {
        yield return new WaitForSeconds(1);

        AsyncOperation ao = SceneManager.LoadSceneAsync("StartingZone");
        ao.allowSceneActivation = false;

        while (!ao.isDone)
        {

            if (ao.progress == 0.9f)
            {
                ao.allowSceneActivation = true;
            }

            // Debug.Log(ao.progress);

            yield return null;
        }
    }
   
    // Use this for initialization
    void Awake () {

        //animator = GetComponent<Animator>();
        canvasGroup = GetComponent<CanvasGroup>();


    }


    void Start()
    {
        //StartCoroutine(FadeIn(1));
    }

    // Update is called once per frame
    void Update () {

        //if(!animator.GetCurrentAnimatorStateInfo(0).IsName("Main_Menu_Open"))
        //{
        //    canvasGroup.blocksRaycasts = canvasGroup.interactable = false;
        //}else
        //{
        //    canvasGroup.blocksRaycasts = canvasGroup.interactable = true;
        //}
	
	}



    IEnumerator FadeIn(float time)
    {

        CanvasRenderer rend = GetComponent<CanvasRenderer>();

        canvasGroup.interactable = false;
        canvasGroup.alpha = 0;



        float alpha = canvasGroup.alpha;
        float alphaRate = 1.0f / 100;

        float amounttime = Time.time;

        Debug.Log(amounttime);


        while (alpha < 1.0f)
        {

            //Debug.Log(canvasGroup.alpha);

            alpha += alphaRate;

            canvasGroup.alpha = alpha;

           // amounttime += Time.deltaTime;

            yield return new WaitForSeconds(time / 100);

        }

        Debug.Log(Time.time - amounttime);

        canvasGroup.interactable = true;

    }


    //public void HHHHHHHHHHHHHEY()
    //{

    //    Debug.Log("HHHHHHHHHHHHHEY");

    //}
}
