﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundManager : MonoBehaviour
{

    public enum SoundList
    {
        GUN,
        LEVEL,
        OGRE,
        ZOMBIE,
        TOTAL
    }

    [System.Serializable]
    public class SoundDict
    {

        public string key;
        public AudioClip value;

    }


    [Header("Zombie Sounds")]
    public List<SoundDict> zombieList;

    [Header("Ogre Sounds")]
    public List<SoundDict> ogreList;

    [Header("Guns Sounds")]
    public List<SoundDict> gunList;

    [Header("Level Sounds")]
    public List<SoundDict> levelList;






    public AudioClip GetClip(string key, SoundList list)
    {

        AudioClip clip = null;

        List<SoundDict> sounds;

        switch (list)
        {
            case SoundList.ZOMBIE:
                sounds = zombieList;
                break;

            case SoundList.OGRE:
                sounds = ogreList;
                break;

            case SoundList.GUN:
                sounds = gunList;
                break;

            case SoundList.LEVEL:
                sounds = levelList;
                break;

            default:
                return null;
        }



        for (int i = 0; i < sounds.Count; i++)
        {

            if (key == sounds[i].key)
            {
                clip = sounds[i].value;
                break;
            }

        }

        return clip;

    }



}
