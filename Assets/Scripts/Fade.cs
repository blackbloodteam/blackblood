﻿using UnityEngine;
using System.Collections;

public class Fade : MonoBehaviour {

    public float fadeTimeSeconds = 3;
    public bool fade_Out = false;

    public float moveTimeSeconds = 1;



    private CanvasRenderer rend;

    // Use this for initialization
    void Start()
    {
        
        rend = GetComponent<CanvasRenderer>();

        StartCoroutine( fadeOut() );
        StartCoroutine( move() );

    }

    IEnumerator fadeOut()
    {
        

        float alpha = rend.GetAlpha();
        float alphaRate = alpha / 100;

        while( alpha > 0.0f)
        {

            alpha -= alphaRate;

            rend.SetAlpha(alpha);

            yield return new WaitForSeconds(fadeTimeSeconds / 100);

        }

        Destroy(gameObject);

    }


    IEnumerator move()
    {

        RectTransform trans = GetComponent<RectTransform>();

        Vector2 position = trans.anchoredPosition;


        int count = 0;

        do
        {

            position.y++;

            trans.anchoredPosition = position;

            yield return new WaitForSeconds(moveTimeSeconds / 20);

            count++;
        } while (count < 20);

    }

    // Update is called once per frame
    void Update () {
	
	}
}
