﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.Events;
using UnityStandardAssets.Characters.FirstPerson;
//using System;

public class GameManager : MonoBehaviour
{

    const float WAVETIMEBASE = 15f;
    const float ENEMYPOWERFOMULABASE = 0.01f;

    [HideInInspector]
    public bool GAMEOVER = false;

    [HideInInspector]
    public static bool paused = false;

    float wavetime = 15f;
    float enemypower = 2f;

    int roundTime = 30;

    private const int SPHERE_POINTS = 10;

    public int level = 1;

    // It can't be in the Zombie because it is constantly spawing new ones.
    private bool hitKillPerk = false;



    private int spheresLeft = 0;
    private int enemiesLeft = 0;

    [HideInInspector]
    public int orbCounter;
    //private int score = 0;


    public GameObject collectibles;

    public SpawnAreas spawnAreas;
    public List<GameObject> enemies = new List<GameObject>();

    public Player player;
    public GunShop[] gunShops;


    private List<PerkLifeTime> perks = new List<PerkLifeTime>();
    private Canvas canvas;

    private bool spawning = false;


    public int getOrbCounter()
    {
        return orbCounter;
    }
    // Use this for initialization
    void Start()
    {

        // Get Canvas script
        canvas = GetComponent<Canvas>();

        // Load spheres on level 
        Instantiate(collectibles, Vector3.zero, Quaternion.identity);

        // Count spheres left
        spheresLeft = CountObjectsInScene(collectibles.transform, "Sphere");

        canvas.UpdateSpheresText(spheresLeft);  // Update Canvas

        StartCoroutine(SpawnWave(roundTime, level, 10, enemies, spawnAreas));

    }


    IEnumerator SpawnWave(int time, int round, int cooldown, List<GameObject> enemyList, SpawnAreas spawnAreasList)
    {

        spawning = true;

        yield return new WaitForSeconds(cooldown); // Wait for start


        int enemies = (Mathf.RoundToInt(Mathf.Pow(round, enemypower)) + 3);

        // Calculate Time for the end of the round
        float endTime = Time.time + (wavetime);

        float spawningWaitTime = wavetime / enemies;

        enemypower -= ENEMYPOWERFOMULABASE;
        wavetime += WAVETIMEBASE;



        while (Time.time < endTime)
        {

            int max = Mathf.Abs(round / 2);
            max = (max != 0) ? max : 1;

            int sameTimeSpawn = UnityEngine.Random.Range(1, max);

            int enemyIndex = UnityEngine.Random.Range(0, enemyList.Count);


            GameObject enemy = enemyList[enemyIndex];
            GameObject spawnLocation = null;

            while (spawnLocation == null)
            {

                int spawnIndex;


                if (enemy.tag == "Ogre")
                {
                    spawnIndex = UnityEngine.Random.Range(0, spawnAreasList.ogreAreas.Count);
                    spawnLocation = spawnAreasList.ogreAreas[spawnIndex];
                }
                else
                {
                    spawnIndex = UnityEngine.Random.Range(0, spawnAreasList.zombiesAreas.Count);
                    spawnLocation = spawnAreasList.zombiesAreas[spawnIndex];
                }

            }

            Instantiate(enemyList[enemyIndex], spawnLocation.transform.position, Quaternion.identity);

            yield return new WaitForSeconds(spawningWaitTime);

            yield return new WaitForSeconds(0);

        }

        level++;

        spawning = false;

    }


    void pauseGame()
    {
        if (!paused)//Time.timeScale == 1)
        {
            Time.timeScale = 0;
            canvas.panels.pauseMenu.SetActive(true);
            canvas.panels.inGamePanel.SetActive(false);
            player.GetComponent<FirstPersonController>().enabled = false;

            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;

            paused = true;
        }
        else if (paused)//Time.timeScale == 0)
        {
            Time.timeScale = 1;
            canvas.panels.pauseMenu.SetActive(false);
            canvas.panels.inGamePanel.SetActive(true);

            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;

            player.GetComponent<FirstPersonController>().enabled = true;

            paused = false;
        }    
    }

    // Update is called once per frame
    void Update()
    {
        if (!GAMEOVER)
        {

            if(Input.GetKeyDown(KeyCode.Escape))
            {
                pauseGame();
            }


            // Check if the level is not already spawning waves
            if (!spawning)
            {
                spawning = true;
                roundTime += (Mathf.RoundToInt(Mathf.Pow(level, enemypower)) + 3) * 4;

                StartCoroutine(SpawnWave(roundTime, level, 15, enemies, spawnAreas));
            }

            CheckPerks();
        }

    }


    private void CheckPerks()
    {

        if (perks.Count > 0)
        {
            // Remove Perks not active
            for (int i = perks.Count - 1; i >= 0; i--)
            {
                if (perks[i].isActive(Time.time) == false)
                {
                    // Make a copy
                    PerkLifeTime perk = perks[i];

                    // Delete from List
                    perks.RemoveAt(i);

                    switch (perk.type)
                    {

                        case PerkType.DOUBLE_POINTS:
                        case PerkType.TRIPLE_POINTS:
                        case PerkType.QUAD_POINTS:

                            player.setPointsPerk(perk.type, false);
                            break;

                        case PerkType.FREE_SHOP:

                            foreach (GunShop gunshop in gunShops)
                            {
                                gunshop.setFreeShop(false);
                            }
                            break;


                        case PerkType.HIT_KILL:
                            hitKillPerk = false;
                            break;

                    }

                }
            }
        }
    }





    public void CollectSphere()
    {
        spheresLeft--;

        orbCounter++;
        player.AddScore(SPHERE_POINTS);

        canvas.UpdateSpheresText(spheresLeft); // Update Canvas

    }

    //-------------------------------------------------------------------
    public void enemyHit(Collider enemyCol, float _damage)
    {
        Enemy enemy = enemyCol.GetComponent<EnemyCollider>().GetParent();

        bool isEnemyAlive;
        int points;
        
        float damage = hitKillPerk ? Enemy.FULL_DAMAGE : _damage;

        if (enemyCol.tag == "Head")
        {
            isEnemyAlive = enemy.TakeDamage(damage * DamageMultiplyer.headShot);

            if (isEnemyAlive)
                points = (int)Points.HIT;
            else
                points = (int)Points.HEADSHOT;
        }
        else
        {
            isEnemyAlive = enemy.TakeDamage(damage);

            if (isEnemyAlive)
                points = (int)Points.HIT;
            else
                points = (int)Points.KILL;
        }

        if (!isEnemyAlive)
            player.AddScore(points);
    }


    private int CountObjectsInScene(Transform _gameobject, string _tag)
    {
        int count = 0;

        Transform[] children = _gameobject.GetComponentsInChildren<Transform>();

        foreach (Transform transf in children)
        {

            if (transf.tag == _tag)
            {
                count++;
            }

        }


        return count;
    }


    /*
     * Add a new Perk to the List.
     * 
     * @ perkAdd: New Park that is being added to the perk list
     * 
     * @ perkRemoveList: Perks that is necessary to be removed before adding the new Perk
     *  
     */
    public void AddPerk(PerkLifeTime perkAdd)
    {

        if (perks.Count > 0)
        {

            // In order to add a perk, it`s necessary to first remove an active perk
            // and all perks of the same type but with a lesser rarity
            for (int i = perks.Count - 1; i >= 0; i--)
            {

                // Look for active perk of the same type
                if (perks[i].Compare(perkAdd))
                {
                    Destroy(perks[i].canvasIcon);
                    perks.RemoveAt(i);
                    continue;
                }

            }
        }

        // Add the new Perk at the end of the List.
        perks.Insert(perks.Count, perkAdd);


        GameObject go = null;

        switch (perkAdd.type)
        {

            case PerkType.DOUBLE_POINTS:

                player.setPointsPerk(perkAdd.type, true);
                go = canvas.addPerk(Canvas.PerksObjects.Perks.DOUBLE_POINTS);
                break;

            case PerkType.TRIPLE_POINTS:

                player.setPointsPerk(perkAdd.type, true);
                go = canvas.addPerk(Canvas.PerksObjects.Perks.TRIPLE_POINTS);
                break;


            case PerkType.QUAD_POINTS:

                player.setPointsPerk(perkAdd.type, true);
                go = canvas.addPerk(Canvas.PerksObjects.Perks.QUAD_POINTS);
                break;


            case PerkType.FREE_SHOP:

                foreach (GunShop gunshop in gunShops)
                {
                    gunshop.setFreeShop(true);
                }

                go = canvas.addPerk(Canvas.PerksObjects.Perks.FREE_SHOP);
                break;


            case PerkType.HIT_KILL:
                hitKillPerk = true;
                go = canvas.addPerk(Canvas.PerksObjects.Perks.HITKILL);
                break;

        }

        perkAdd.canvasIcon = go;

    }


    public void UsePerk(PerkType _perkType)
    {


        switch (_perkType)
        {

            case PerkType.FULL_AMMO:

                player.mygun.GetComponent<Gun>().MaxAmmo();
                canvas.UpdatePerkText("Max Ammo");

                break;


            case PerkType.DOUBLE_HEALTH:

                break;


            case PerkType.HEALTH_RESTORE:
                player.RestoreLife();
                canvas.UpdatePerkText("Full Health");
                break;

        }

    }

}
