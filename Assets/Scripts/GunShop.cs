﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GunShop : Shop
{

    public GameObject gun;

    void OnTriggerEnter(Collider other)
    {

        // When player get inside the collider, show buying or repair message
        if (other.tag == "Player")
        {
            trigguered = true;
            canvas.ShowBuyMessage(gunName, real_price);
        }

    }

    public override void ActionAfterBuying()
    {
        player.SetGun(gun);
        canvas.ShowBuyMessage(gunName, real_price);
    }

}
