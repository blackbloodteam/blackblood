﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Gate : MonoBehaviour {

    private const float TIME_TO_BUY = 2.0f;

    public GameManager gameManager;
    public Canvas canvas;

    public OffMeshLink[] navMeshLinks;

    [Space]
    public string gate_name;
    public int price;
    private Animator anim;

    private Player player;

    private int real_price;          
    private float buyingTimeEnd = 0;    
    private float buyingTimeStart = 0;   
    private bool isBuying = false;

    private bool updated;

    //public Enemy newEnemy;
    public GameObject newEnemy;
    public SpawnAreas spawnAreas;



    // Use this for initialization
    void Start () {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

        real_price = price;
        anim = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
	    if(gameManager.getOrbCounter() >= price && !updated)
        {
            canvas.updateObjective(Objectives.OPENGATE);
            updated = true;
        }
	}

    

    void OnTriggerEnter(Collider other)
    {

        // When player get inside the collider, show buying or repair message
        if (other.tag == "Player")
        {
            int orbsRemaining = price - gameManager.getOrbCounter();
            canvas.ShowBuyMessage(gate_name, (orbsRemaining > 0) ? orbsRemaining : 0, true);
        }

        // If player is fast enough to hold button, start buying
        if (Input.GetButtonDown("GunBuying"))
        {

            // Check if player has enough money
            // if (player.GetScore() >= real_price)
            if (gameManager.getOrbCounter() >= price)
            {
                
                buyingTimeStart = Time.time;
                buyingTimeEnd = buyingTimeStart + TIME_TO_BUY;
                isBuying = true;

            }
            else
            {
                /*************************************************
                 * 
                 * NEED CODE FOR NOT ENOUGH MONEY
                 * 
                 *************************************************/
            }
        }

    }


    void OnTriggerStay(Collider other)
    {

        // Check if player is not buying yet
        if (!isBuying)
        {
            if (Input.GetButtonDown("GunBuying"))
            {
                // Check if player has enough money
                if (gameManager.getOrbCounter() >= price)
                {

                    buyingTimeStart = Time.time;
                    buyingTimeEnd = buyingTimeStart + TIME_TO_BUY;
                    isBuying = true;

                }
                else
                {
                    /*************************************************
                     * 
                     * NEED CODE FOR NOT ENOUGH MONEY
                     * 
                     *************************************************/
                }
            }
        }


        // Player release the buying button. Cancel puchase
        if (Input.GetButtonUp("GunBuying"))
        {
            buyingTimeStart = buyingTimeEnd = 0;
            canvas.DisableBuyProgress();
            isBuying = false;
        }


        // Time for buying is over. Finish purchase
        if (isBuying && Time.time >= buyingTimeEnd)
        {

            buyingTimeStart = buyingTimeEnd = 0;
            canvas.DisableBuyProgress();
            canvas.DisableBuyMessage();
            isBuying = false;
            anim.SetBool("isOpen", true);
            GetComponent<BoxCollider>().enabled = false;
            canvas.updateObjective(Objectives.COLLECTORBS);

            foreach (OffMeshLink link in navMeshLinks)
            {
                link.activated = true;
            }

            /*************************************************
             * 
             * NEED CODE FOR NOT ENOUGH MONEY
             * 
             *************************************************/
        }

        // Update buying-time bar
        else if (isBuying)
        {
            canvas.ShowBuyProgress((Time.time - buyingTimeStart) / (TIME_TO_BUY / 100) / 100);
        }
    }


    void OnTriggerExit(Collider other)
    {

        // Cancel any buying
        buyingTimeStart = buyingTimeEnd = 0;
        isBuying = false;

        canvas.DisableBuyProgress();
        canvas.DisableBuyMessage();

    }

    public void stopDuplicates(List<GameObject> origin, List<GameObject> destination)
    {
        for (int i = 0; i < origin.Count; i++)
        {
            for (int n = 0; n < destination.Count; n++)
            {

                if (origin[i].GetInstanceID() == destination[n].GetInstanceID())
                    break;

                if (n == destination.Count - 1)
                {
                    destination.Add(origin[i]);
                }
            }
        }

    }
    public void addEnemiesAndSpawnPoints(){

        if(newEnemy != null)
            gameManager.enemies.Add(newEnemy);

        stopDuplicates(spawnAreas.zombiesAreas, gameManager.spawnAreas.zombiesAreas);
        stopDuplicates(spawnAreas.ogreAreas, gameManager.spawnAreas.ogreAreas);

    }
}
