﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class PerkCanvas : MonoBehaviour {


    public float TIME_TO_FADE = 15.0f;


    // Use this for initialization
    void Start () {

        StartCoroutine(FadeOut());
	
	}

    private IEnumerator FadeOut()
    {

        CanvasRenderer rend = GetComponent<CanvasRenderer>();
        Image image = GetComponent<Image>();


        float startingTime = Time.time;
        float fillAmount = image.fillAmount;

        
        while (image.fillAmount > 0.0f)
        {

            image.fillAmount = 1 - ((Time.time - startingTime) / (TIME_TO_FADE / 100) / 100); ;

            yield return new WaitForEndOfFrame();

        }

        Destroy(gameObject);

    }

}
