﻿using UnityEngine;
using System.Collections;

public class Crawler : Enemy
{

    private bool exausted;
    private bool isLeapOnCooldown;
    private bool isLeaping;

	private float BASE_HEALTH = 50;

    public float leapCooldownTime = 20.0f;

    // Use this for initialization
//	    void Awake()
//	    {
//	        exausted = false;
//	        isLeaping = false;
//	        isLeapOnCooldown = false;
//	        dyingAnimation = "idle";
//
//	        health = MAX_HEALTH;
//	    }


	public override void StartEnemy (int round)
	{

		exausted = false;
		isLeaping = false;
		isLeapOnCooldown = false;

		dyingAnimation = "fall";
		health = BASE_HEALTH + ( round );

		navAgent.speed += round;
		anim.speed += round;

	}


    bool leapRange()
    {
        return Vector3.Distance(transform.position, player.transform.position) <= navAgent.stoppingDistance * 6
                && Vector3.Distance(transform.position, player.transform.position) >= navAgent.stoppingDistance * 1.2;
    }

    bool attackInRange()
    {
        return Vector3.Distance(transform.position, player.transform.position) <= navAgent.stoppingDistance * 1.2;
    }

    private void leapAttack()
    {
        navAgent.speed = 10;
        anim.Play("pounce");

        StartCoroutine(inLeap());
    }

    IEnumerator inLeap()
    {
        isLeaping = true;
        yield return new WaitForSeconds(1);
        isLeaping = false;
    }
    IEnumerator leapCooldown()
    {
        isLeapOnCooldown = true;
        yield return new WaitForSeconds(leapCooldownTime);
        isLeapOnCooldown = false;
    }

    private void defaultSpeed()
    {
        navAgent.speed = 2;
    }

    private void sprint()
    {
        float sprintSpeed = 4;

        navAgent.speed = sprintSpeed;
        anim.Play("crawl_fast");
        StartCoroutine(sprintDuration());
    }

    IEnumerator sprintDuration()
    {
        yield return new WaitForSeconds(5);
        StartCoroutine(sprintCooldown());
    }

    IEnumerator sprintCooldown()
    {
        exausted = true;
        yield return new WaitForSeconds(20);
        exausted = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (alive)
        {
            navAgent.SetDestination(player.transform.position);
     
            if (!attackInRange())
            {
                if (leapRange() && !isLeapOnCooldown && !isLeaping)
                {
                    leapAttack();
                    StartCoroutine(leapCooldown());
                }
                else
                {
                    if (!isLeaping && exausted)
                    {
                        defaultSpeed();
                    }

                    if (!anim.GetCurrentAnimatorStateInfo(0).IsName("pounce") && exausted)
                        anim.Play("crawl");
                }

                 if(!exausted && !isLeaping)
                {
                    sprint();
                }
                 else if(!isLeaping)
                {
                    defaultSpeed();
                }

            }
            else if(!isLeaping)
            {
                anim.Play("attack");
            }
        }
    
        else
        {

            if (!dropCheck)
            {

                if (!anim.GetCurrentAnimatorStateInfo(0).IsName(dyingAnimation))
                {

                    dropManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<Drop>();

                    // Check Drop
                    Perk perk = dropManager.getDrop(dropRate);

                    // Do not check it again
                    dropCheck = true;

                    if (perk != null && perk.weight != PerkWeight.NONE)
                    {

                        Instantiate(perk.gameObject, transform.position + Vector3.up, perk.gameObject.transform.rotation);

                    }
                }
            }
        }
    }

    protected override AudioClip GetAudioClip(SOUND_TYPES type) {
    
        return soundmanager.GetClip("ATTACK", SoundManager.SoundList.TOTAL);
    }

}
