﻿using UnityEngine;
using System.Collections;

public class Rhino : Enemy {

    private float MAX_HEALTH = 200;
	// Use this for initialization
	void Awake () {

        health = MAX_HEALTH;
	}

	public override void StartEnemy (int round)
	{


	}
	
	// Update is called once per frame
	void Update () {
	
	}

    protected override AudioClip GetAudioClip(SOUND_TYPES type)
    {

        return soundmanager.GetClip("ATTACK", SoundManager.SoundList.TOTAL);
    }

}
