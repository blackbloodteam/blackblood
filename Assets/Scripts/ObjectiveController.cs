﻿using UnityEngine;
using System.Collections;

public class ObjectiveController : MonoBehaviour {

    public Canvas canvas;
    public Player player;

    private bool prompted = false;
	// Use this for initialization
	void Start () {

        canvas.updateObjective(Objectives.FIRSTOBJ);
    }

    // Update is called once per frame
    void Update()
    {

        if (player.score >= 600 && !prompted)
        {
            canvas.updateObjective(Objectives.BUYGUN);
            prompted = true;
            StartCoroutine(endTutorialObjectives());
        }
    }
        
        //remove constant updating for no reason
        IEnumerator endTutorialObjectives()
            {
            yield return new WaitForSeconds(5);
            Destroy(this);
            }
}
