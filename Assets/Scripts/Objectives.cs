﻿using UnityEngine;
using System.Collections;

public class Objectives {

    public const string FIRSTOBJ = "You Are Unarmed! Gather orbs to buy a weapon!";
    public const string BUYGUN = "Objective: Buy your first weapon!";
    public const string COLLECTORBS = "Objective: Gather all the orbs around the area!";
    public const string OPENGATE = "Objective: Open the Gate!";
}
