﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class SpawnAreas
{

    public List<GameObject> zombiesAreas = new List<GameObject>();
    public List<GameObject> ogreAreas = new List<GameObject>();

}