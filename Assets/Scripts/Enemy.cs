﻿using UnityEngine;
using System.Collections;
using System;

public abstract class Enemy  : MonoBehaviour {



    public enum SOUND_TYPES
    {

        ATTACK,
        DIE,
        ROAR,
        WALK,
        TOTAL

    }



    protected SoundManager soundmanager;
    protected AudioSource source;



    // General Attributes
    [HideInInspector]public float level;
    [HideInInspector]public bool alive;
	public int damage;
	[HideInInspector]public float health;
	public int dropRate;



    public Animator anim;


    [HideInInspector]public bool dropCheck = false;

    [HideInInspector]public const float FULL_DAMAGE = -1;

    [HideInInspector]public Drop dropManager;
    
    public GameObject[] colliders;

    [HideInInspector]public GameObject player;

    [HideInInspector]public GameManager gameManager;

    [HideInInspector]public NavMeshAgent navAgent;

    [HideInInspector]public float attackDistance;

    [HideInInspector]public string dyingAnimation;

    [HideInInspector] public static bool attackSound;
    

    public abstract void StartEnemy(int round);
    protected abstract AudioClip GetAudioClip(SOUND_TYPES type);


    // Use this for initialization
    void Start () {
        alive = true;
        anim = GetComponent<Animator>();
        navAgent = GetComponent<NavMeshAgent>();
        player = GameObject.FindGameObjectWithTag("Player");

        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        soundmanager = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>();
        source = GetComponent<AudioSource>();

        StartEnemy (gameManager.level);
    }

    IEnumerator soundManagement()
    {
        
        attackSound = true;
        yield return new WaitForSeconds(3);
        attackSound = false;
    }

    public bool attackInRange()
    {
        return Vector3.Distance(transform.position, player.transform.position) <= navAgent.stoppingDistance * attackDistance;
    }

    public void dealDamage()
    {
        if(attackInRange())
            player.GetComponent<Player>().takeDamage(damage);
        if(!attackSound)
        {

            source.PlayOneShot(GetAudioClip(SOUND_TYPES.ATTACK));

            StartCoroutine(soundManagement());
        }
    }

    public bool TakeDamage(float damage)
    {
        if (alive)
        {
            // Check if it`s receiving FULL DAMAGE due HitKill Perk
            if (damage == FULL_DAMAGE)
                health = 0;
            else
                health -= damage;
            // Check if it`s dead
            if (health <= 0)
            {
                die(dyingAnimation);
            }
        }
        return alive;
    }

    public void die(string dieAnimation)
    {

        foreach (GameObject collider in colliders)
        {
            collider.SetActive(false);
        }

        alive = false;
        anim.Play(dieAnimation);
        navAgent.Stop();

        if (!this.anim.GetCurrentAnimatorStateInfo(0).IsName(dieAnimation))
        {
            Destroy(this.gameObject, 15);
        }
    }



    protected string GetKey(SOUND_TYPES type)
    {

        switch (type)
        {
            case SOUND_TYPES.ATTACK:    return "ATTACK";
            case SOUND_TYPES.DIE:       return "DIE";
            case SOUND_TYPES.ROAR:      return "ROAR";
            case SOUND_TYPES.WALK:      return "WALK";
            default:                    return null;
        }

    }

}
