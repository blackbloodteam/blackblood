﻿using UnityEngine;
using System.Collections;

public class SpawnPoint : MonoBehaviour {

	public bool active;

	private float endTime;

    // Use this for initialization
    void Start () {

		active = true;
	
	}


	IEnumerator CoolDown(int secs){

		Debug.Log (gameObject.name + " active = false ");

		yield return new WaitForSeconds (secs);

		Debug.Log (gameObject.name + " active = true ");

		active = true;

	}


	public bool GetActive(){
		return active;
	}

	public void SetActive(bool value){

		active = value;

		if (value == false) {

			endTime = Time.time + 0.1f;

		}
	}



	void Update(){


		if (active == false) {
			if (Time.time >= endTime) {
				active = true;
			}

		}
	}
}
