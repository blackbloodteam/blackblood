﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;

public class Canvas : MonoBehaviour
{

    public GameObject canvasOBJ;
    public Player player;


    [System.Serializable]
    public class Panels
    {
        public GameObject inGamePanel;
        public GameObject gameOverPanel;
        public GameObject pauseMenu;
    }



    [System.Serializable]
    public class Texts
    {
        public Text score;
       // public Text spheresLeft;
        public Text roundAmmoLeft;
        public Text maxAmmoLeft;
        public Text buyText;
        public Text health;
        public Text perkMessage;
        public Text objectiveMessage;

        public Text roundsSurvived;
    }

    [System.Serializable]
    public class ProgressBars
    {
        public Scrollbar buyBar;
        public Scrollbar fireRateBar;
    }

    [System.Serializable]
    public class PerksObjects
    {

        public enum Perks { DOUBLE_POINTS, TRIPLE_POINTS, QUAD_POINTS, FREE_SHOP, HITKILL, INVENCIBLE }

        public GameObject doublePoints;
        public GameObject triplePoints;
        public GameObject quadPoints;
        public GameObject freeShop;
        public GameObject hitKill;
        public GameObject invencible;

        public Transform parent;

    }


    public Panels panels;
    public Texts texts;
    public ProgressBars progress;
    public PerksObjects perks;

    public Text textAnimation;



    public void exitApplication()
    {
        Application.Quit();
    }

    public void BackMainMenu()
    {
        Time.timeScale = 1.0f;
        SceneManager.LoadScene("Main_Menu");
    }


    public void resumeGame()
    {
        if (GameManager.paused)//Time.timeScale == 0)
        {
            Time.timeScale = 1;
            panels.pauseMenu.SetActive(false);
            panels.inGamePanel.SetActive(true);
            player.GetComponent<FirstPersonController>().enabled = true;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            GameManager.paused = false;
        }
    }

    public void SetRoundsSurvivedText(int _rounds)
    {
        texts.roundsSurvived.text += "<color=yellow>" + _rounds.ToString() + "</color>";
    }



    public void UpdateHealthText(int _health)
    {
        texts.health.text = _health.ToString();

        if (_health >= 70)
        {
            texts.health.color = Color.green;
        }
        else if (_health >= 30)
        {
            texts.health.color = Color.yellow;
        }
        else
        {
            texts.health.color = Color.red;
        }
    }

    public void UpdateSpheresText(int _spheresLeft)
    {
        //texts.spheresLeft.text = _spheresLeft.ToString("D3");
    }

    public void UpdateScoreText(int _score)
    {
        texts.score.text = _score.ToString();
    }

    public void UpdateRoundAmmoText(int _ammoCount)
    {
        texts.roundAmmoLeft.text = _ammoCount.ToString("D2");
    }

    public void UpdateMaxAmmoText(int _ammoCount, bool lastRound)
    {
        texts.maxAmmoLeft.text = _ammoCount.ToString("D3");

        if (lastRound)
        {
            texts.maxAmmoLeft.color = Color.red;
        }
        else
        {
            texts.maxAmmoLeft.color = Color.yellow;
        }

    }

    public void ShowBuyMessage(string _objName, int _price, bool isGate = false)
    {
        string style;

        if (!isGate)
        {

            if (player.score >= _price)
            {
                style = "<color=green>";
            }
            else
            {
                style = "<color=red>";
            }

            texts.buyText.text = _objName + " - Price: " + style + _price.ToString() + "</color>\n";
        }
        else
        {

            if (_price == 0)
            {
                style = "<color=green>";
            }
            else
            {
                style = "<color=red>";
            }

            texts.buyText.text = _objName + ": " + style + _price.ToString() + "</color>\n";
        }

        texts.buyText.text += "Hold [Q] to buy";

        texts.buyText.enabled = true;
    }

    public void DisableBuyMessage()
    {
        texts.buyText.text = "xxx";
        texts.buyText.enabled = false;
    }

    public void ShowBuyProgress(float value)
    {

        progress.buyBar.gameObject.SetActive(true);

        progress.buyBar.size = value;

    }



    public void ShowFireRateProgress(float value)
    {

        progress.fireRateBar.size = value;

    }

    public void DisableBuyProgress()
    {
        progress.buyBar.gameObject.SetActive(false);
        progress.buyBar.size = 0;
    }






    // Animations

    public void addScoreTextAnimation(int _points)
    {

        Vector3 position = new Vector3(0, 0, 0);

        Text temp = Instantiate(textAnimation) as Text;

        temp.text = (_points > 0) ? "+" + _points.ToString() : _points.ToString();

        temp.rectTransform.SetParent(canvasOBJ.transform, false);

        position.x = Random.Range(texts.score.rectTransform.anchoredPosition.x - 30,
                                    texts.score.rectTransform.anchoredPosition.x + 30);
        position.y = texts.score.rectTransform.anchoredPosition.y + texts.score.rectTransform.sizeDelta.y;

        temp.rectTransform.anchoredPosition = new Vector2(position.x,
            texts.score.rectTransform.anchoredPosition.y - texts.score.rectTransform.sizeDelta.y);
    }



    public void UpdatePerkText(string text)
    {
        texts.perkMessage.text = text;
        //texts.perkMessage.GetComponent<CanvasRenderer>().SetAlpha(1.0f);
        StartCoroutine(fadeOut(texts.perkMessage));
    }

    public void updateObjective(string text)
    {
        texts.objectiveMessage.text = text;
        StartCoroutine(fadeOut(texts.objectiveMessage));
    }

    IEnumerator fadeOut(Text text)
    {


        CanvasRenderer rend = text.GetComponent<CanvasRenderer>();
        rend.SetAlpha(1.0f);
        float alpha = 1.0f;
        float alphaRate = alpha / 100;

        while (alpha > 0.0f)
        {

            alpha -= alphaRate;

            rend.SetAlpha(alpha);

            yield return new WaitForSeconds(3.5f / 100);

        }

        rend.SetAlpha(0.0f);

        // Destroy(gameObject);

    }



    public GameObject addPerk(PerksObjects.Perks perk)
    {

        GameObject go = null;

        switch (perk)
        {
            case PerksObjects.Perks.DOUBLE_POINTS:
                go = Instantiate(perks.doublePoints) as GameObject;
                UpdatePerkText("Double Points");
                break;

            case PerksObjects.Perks.TRIPLE_POINTS:
                go = Instantiate(perks.triplePoints) as GameObject;
                UpdatePerkText("Triple Points");
                break;

            case PerksObjects.Perks.QUAD_POINTS:
                go = Instantiate(perks.quadPoints) as GameObject;
                UpdatePerkText("Quad Points");
                break;

            case PerksObjects.Perks.FREE_SHOP:
                go = Instantiate(perks.freeShop) as GameObject;
                UpdatePerkText("Free Shop");
                break;

            case PerksObjects.Perks.HITKILL:
                go = Instantiate(perks.hitKill) as GameObject;
                UpdatePerkText("One Shot");
                break;

            case PerksObjects.Perks.INVENCIBLE:
                go = Instantiate(perks.invencible) as GameObject;
                UpdatePerkText("");
                break;
        }

        if (go)
        {
            go.transform.parent = perks.parent;
        }

        return go;

    }
}
