﻿using UnityEngine;
using System.Collections;
using System;

public class Gun : MonoBehaviour {

    public LayerMask layer;

    [Serializable]
    public class Particles
    {
        public Transform spawnPosition;
        public GameObject particleEffect;
    }

    public Particles[] particles;
    

    public float damage;
    public float fireRate;
    public float range;
    public int roundBox;
    public int maxAmmo;
    public float reloadTime; 
    public float recoil;



    private GameManager gameManager;
    private Canvas canvas;
    private Animator animator;

    private float lastShot;
    private float nextShot;
    private int actualRoundBox;
    private int actualMaxAmmo;

    private bool reload;
    private float reloadFinished;


    void Start()
    {

        GameObject gmanager = GameObject.FindGameObjectWithTag("GameController");

        actualRoundBox = roundBox;
        actualMaxAmmo = maxAmmo;
        reload = false;

        gameManager = gmanager.GetComponent<GameManager>();
        canvas = gmanager.GetComponent<Canvas>();
        animator = GetComponent<Animator>();

        //Update Canvas
        canvas.UpdateRoundAmmoText(actualRoundBox);
        canvas.UpdateMaxAmmoText(actualMaxAmmo, false);

    }

    void Update()
    {

        if (gameManager.GAMEOVER)
            return;


        if( Time.time <= nextShot )
            canvas.ShowFireRateProgress((Time.time - lastShot) / (fireRate / 100) / 100);
        //Debug.Log((nextShot - lastShot) / (fireRate / 100) / 100);




        if ( Input.GetButton("Fire1"))
        {
            Shoot();
        }

        if (Input.GetButtonDown("Reload"))
        {
            Reload();
        }


        // Check if it's realoading
        if ( isReloading() )
        {
            // Check if the time for reaload is done.
            if( Time.time >= reloadFinished)
            {
                reload = false;

                RefillTheRoundBox();

            }
        }

    }


    public void Shoot()
    {

        // Check Fire Rate and if the gun is not Reloading
        if (Time.time >= nextShot && !isReloading())
        {

            // 
            if (isThereAmmo())
            {

                // That`s gonna be the last shot time in order to give feedback on canvas for the next shot
                lastShot = Time.time;

                // Set new Time to Shoot
                nextShot = Time.time + fireRate;

                // Reset Bar
                canvas.ShowFireRateProgress(0.0f);

                animator.Play("Shoot");

                actualRoundBox--;
                canvas.UpdateRoundAmmoText(actualRoundBox);
                

                Vector3 origin = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0.0f));
                Vector3 direction = Camera.main.transform.forward;
                RaycastHit hit;

                // if (Physics.Raycast(origin, direction, out hit, 70.0f, layer))
                Physics.Raycast(origin, direction, out hit, range);
                
                // Check if it`s collide with something to set a shot direction
                if(hit.collider == null)
                {
                    hit.point = Camera.main.transform.position + Camera.main.transform.forward * range;
                }

                // Instatiate Shots
                foreach (Particles particle in particles)
                {
                    GameObject go = Instantiate(particle.particleEffect, particle.spawnPosition.position, 
                                    Quaternion.LookRotation( hit.point - particle.spawnPosition.position)) as GameObject;


                    if(go.tag == "Shot")
                    {
                        go.GetComponent<Projectile>().damage = damage;
                    }

                }

            }
        }
    }


    public void Reload()
    {
        // Check if there's more ammo to reload
        if ( actualMaxAmmo > 0 && actualRoundBox < roundBox) {
            reload = true;
            reloadFinished = Time.time + reloadTime;
            animator.Play("Reload");
        }
    }


    public bool isReloading()
    {
        return reload;
    }


    public bool isThereAmmo()
    {
        // Check if there's ammo in the RoundBox
        if ( actualRoundBox == 0 )
        {
            Reload();
            return false;
        }

        return true;
    }


    private void RefillTheRoundBox()
    {
        // Check how many ammo is needed to fullfil the Box
        int ammodif = roundBox - actualRoundBox;

        // Check if there's enough ammo in the gun
        if (actualMaxAmmo >= ammodif)
        {
            actualMaxAmmo -= ammodif;        // Remove the difference from the Max Ammo
            actualRoundBox = roundBox;  // Fullfill the Mag
        }
        else
        {
            actualRoundBox = actualMaxAmmo; // Fill the Mag with the rest of the ammo
            actualMaxAmmo = 0;                   // Empty the Max Ammo
        }

        //Update Canvas
        canvas.UpdateRoundAmmoText(actualRoundBox);
        canvas.UpdateMaxAmmoText(actualMaxAmmo,  (actualMaxAmmo <= roundBox) ? true : false  );

    }

	public void MaxAmmo(){

		actualMaxAmmo = maxAmmo;

		canvas.UpdateMaxAmmoText(actualMaxAmmo, false);

	}


}
