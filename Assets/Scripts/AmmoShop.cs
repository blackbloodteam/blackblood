﻿using UnityEngine;
using System.Collections;

public class AmmoShop : Shop
{


	void OnTriggerEnter(Collider other)
	{




		// When player get inside the collider, show buying or repair message
		if (other.tag == "Player") {

			if (player.mygun != null) {

				trigguered = true;
				canvas.ShowBuyMessage (gunName, real_price);
			}
		}

	}



	public override void ActionAfterBuying(){

		if (player.mygun != null) {

			Gun gun = player.mygun.GetComponent<Gun> ();

			gun.MaxAmmo ();

		}
	}
}

