﻿using UnityEngine;
using System.Collections;
using System;

public class PerkLifeTime {


    public readonly PerkType type;
    private bool timeBased;
    private float expireTime;
    public GameObject canvasIcon
    {
        get;
        set;
    }


    public PerkLifeTime(PerkType _type, bool _timeBased = true)
    {

        type = _type;
        timeBased = _timeBased;

        if (timeBased)
            expireTime = Time.time + 15.0F;

    }



    public bool isActive(float _actualTime)
    {

        // Non-time based perks will always be active until they are needed to be used.
        // Time based perks will only be active while they time has not been expired
        if (timeBased)
        {

            // Is it still active?
            if (expireTime >= _actualTime)
                return true;
            else
                return false;

        }
        else
            return true;

    }


    public bool Compare(PerkType _type)
    {

        if (type == _type)
            return true;
        else
            return false;

    }

    public bool Compare(PerkLifeTime perk)
    {

        if (type == perk.type)
            return true;
        else
            return false;

    }

    //public void setCanvasIcon(GameObject go)
    //{
    //    canvasIcon = go;
    //}



    //public static bool operator ==(PerkLifeTime _1, PerkLifeTime _2)
    //{

    //    if (_1.type == _2.type)
    //        return true;
    //    else
    //        return false;

    //}

    //public static bool operator !=(PerkLifeTime _1, PerkLifeTime _2)
    //{

    //    if (_1.type != _2.type)
    //        return true;
    //    else
    //        return false;

    //}


}
