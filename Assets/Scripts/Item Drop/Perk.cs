﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using System.Collections.Generic;

public class Perk : MonoBehaviour {



    public float lifeTime; // How long it will last

    public PerkWeight weight; // Weight for drop

    public UnityEvent action;  // Method it has to call when Trigguered


    private GameManager gameManager; // For adding perks

    private Material material; // For Fading out


    // Use this for initialization
    void Start () {

        material = GetComponent<Renderer>().material;

        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();

    }
	
	// Update is called once per frame
	void Update () {

        transform.Rotate(new Vector3(0, 1, 0), Space.World);

        // Fading out
        material.color = new Color( material.color.r,
                                    material.color.g,
                                    material.color.b,
                                    material.color.a - (Time.deltaTime / lifeTime));

        // Destroy Game Object when it`s not visible anymore
        if (material.color.a <= 0.0F)
        {
            GetComponent<SphereCollider>().enabled = false;
            //Destroy(transform.parent.gameObject);
            Destroy(gameObject);
        }


    }


    private void OnTriggerEnter(Collider other)
    {
            
        if(other.tag == "Player")
        {
            action.Invoke();

            Destroy(gameObject);
        }

    }



    /*************************
     *   COMMON PERKS
     *************************/

    public void DoublePoints()
    {

        gameManager.AddPerk(new PerkLifeTime(PerkType.DOUBLE_POINTS));

    }


    public void FullAmmo()
    {

        gameManager.UsePerk(PerkType.FULL_AMMO);

    }


    public void DoubleHealth()
    {

        gameManager.UsePerk(PerkType.DOUBLE_HEALTH);

    }


    public void HealthRestore()
    {

        gameManager.UsePerk(PerkType.HEALTH_RESTORE);

    }


    /*************************
     *   RARE PERKS
     *************************/

    public void TriplePoints()
    {

        gameManager.AddPerk(new PerkLifeTime(PerkType.TRIPLE_POINTS));

    }


    public void HitKill()
    {

        gameManager.AddPerk(new PerkLifeTime(PerkType.HIT_KILL));
    }


    public void FreeShop()
    {

        gameManager.AddPerk(new PerkLifeTime(PerkType.FREE_SHOP));
    }


    public void ReviveHalf()
    {


    }


    /*************************
     *   EPIC PERKS
     *************************/

    public void QuadPoints()
    {

        gameManager.AddPerk(new PerkLifeTime(PerkType.QUAD_POINTS));//, removePerks);

    }


    public void Invencible()
    {

        gameManager.AddPerk(new PerkLifeTime(PerkType.INVENCIBLE));

    }


    public void ReviveFull()
    {


    }


}
