﻿using UnityEngine;
using System.Collections;

public enum PerkWeight
{

    COMMON = 5,

    RARE = 3,

    EPIC = 1,

    NONE = 90

}