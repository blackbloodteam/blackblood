﻿using UnityEngine;
using System.Collections;

public enum PerkType {

   DOUBLE_POINTS,
   FULL_AMMO,
   DOUBLE_HEALTH,
   HEALTH_RESTORE,


   TRIPLE_POINTS,
   HIT_KILL,
   FREE_SHOP,
   REVIVE_HALF,


   QUAD_POINTS,
   INVENCIBLE,
   REVIVE_FULL

}
