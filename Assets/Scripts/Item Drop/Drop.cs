﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Drop : MonoBehaviour {



    public List<Perk> perkList = new List<Perk>();


   // private UnityEngine.Random random;// = new UnityEngine.Random();
    private int total_weight;


    void Start()
    {

        UnityEngine.Random.seed = DateTime.Now.Millisecond;

        foreach ( Perk perk in perkList)
        {
            total_weight += (int)perk.weight;
        }
    }



    public Perk getDrop( int dropRate )
    //public GameObject getDrop( int dropRate )
    {
        Perk chosen = null;
        int local_weight = 0;



        int check_drop = UnityEngine.Random.Range(1, 100);

        if (check_drop > dropRate)
        {

            return null;
        }


        int value = (int)Math.Round((UnityEngine.Random.value * (total_weight - 1)) + 1, MidpointRounding.AwayFromZero); // Get Value


        foreach (Perk perk in perkList)
        {

            local_weight += (int)perk.weight;

            // Is it the picked one?
            if (value <= local_weight)
            {
                chosen = perk;
                break;
            }

        }


        return chosen;

    }


}
