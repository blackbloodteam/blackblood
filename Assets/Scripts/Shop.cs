﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public abstract class Shop : MonoBehaviour {
	/*
     * Constants
     */
	private const float TIME_TO_BUY = 2.0f;

	/*
     * Public Objects
     */
	public GameManager gameManager;

	public Canvas canvas;

	/*
     * Public attributes
     */
	public string gunName;     // Name that will show on the screen
	public int price;          // Base price of a new gun

	/*
     * Private Objects
     */
	protected Player player;

	/*
     * Private attibutes
     */
	protected int real_price;              // The price that will show on the screen
	private float buyingTimeEnd = 0;     // Time expect to finish buying
	private float buyingTimeStart = 0;   // Time player started buying
	private bool isBuying = false;       // Flag to avoid rebuying over and over
    private bool firstBuy = true;


	protected bool trigguered = false;


	void Start () {

		player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

		real_price = price; // Set initial price

	}


	void Update () {

		transform.Rotate(new Vector3(0, 1, 0), .5f);


		if (trigguered) {

			// If player is fast enough to hold button, start buying
			if ( Input.GetButtonDown("GunBuying") && !isBuying )
			{
				//Debug.Log ("ButtonDown");

				// Check if player has enough money
				if (player.GetScore() >= real_price)
				{
					isBuying = true;


					buyingTimeStart = Time.time;
					buyingTimeEnd = buyingTimeStart + TIME_TO_BUY;


				}
				else
				{

                    //Debug.Log("No money");
					/*************************************************
                 * 
                 * NEED CODE FOR NOT ENOUGH MONEY
                 * 
                 *************************************************/
				}
			}


			if ( Input.GetButtonUp("GunBuying") && isBuying )
			{
				isBuying = false;

				//Debug.Log ("ButtonUp");

				buyingTimeStart = buyingTimeEnd = 0;
				canvas.DisableBuyProgress();

			}




			// Time for buying is over. Finish purchase
			if( isBuying && Time.time >= buyingTimeEnd)
			{

				buyingTimeStart = buyingTimeEnd = 0;
				canvas.DisableBuyProgress();
				isBuying = false;

				// Remove points from player
				player.AddScore(-real_price);


				/*************************************************
             * 
             * NEED CODE FOR NOT ENOUGH MONEY
             * 
             *************************************************/
				ActionAfterBuying();

                if (firstBuy)
                {
                    canvas.updateObjective(Objectives.COLLECTORBS);
                    firstBuy = false;
                }


            }

			// Update buying-time bar
			else if (isBuying)
			{
				canvas.ShowBuyProgress((Time.time - buyingTimeStart) / (TIME_TO_BUY / 100) / 100);
			}

		} else {


			// Cancel any buying
			buyingTimeStart = buyingTimeEnd = 0;
			isBuying = false;


		}







		// Player release the buying button. Cancel puchase








	}

	void OnTriggerExit(Collider other)
	{

		if (other.tag == "Player") {
			trigguered = false;
			canvas.DisableBuyProgress();
			canvas.DisableBuyMessage ();
		}

	}


	// For FreeShop perk
	public void setFreeShop(bool value)
	{
		if (value)
			real_price = 0;
		else
			real_price = price;
	}


	public abstract void ActionAfterBuying ();

}
