﻿using UnityEngine;
using System.Collections;

public class EnemyCollider : MonoBehaviour {

    public Enemy parent;


    public Enemy GetParent()
    {
        return parent;
    }


    void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "Shot")
        {

            Projectile shot = other.gameObject.GetComponent<Projectile>();
            float multiplyer = 1;

            if (this.gameObject.tag == "Head")
            {
                multiplyer = DamageMultiplyer.headShot;
            }

            parent.gameManager.enemyHit(this.GetComponent<Collider>(), shot.damage);

            Destroy(other.gameObject);

        }

    }

}
